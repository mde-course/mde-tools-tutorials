This repository contains the material developed during the recording of videos
on MDE Tools, available at:

https://www.youtube.com/playlist?list=PL0g6tB8MgpmZRr_82yG_dSdNvx0LmNuCl

Copyright (c) 2023 Leonardo Montecchi  
https://www.ntnu.no/ansatte/leonarmo
