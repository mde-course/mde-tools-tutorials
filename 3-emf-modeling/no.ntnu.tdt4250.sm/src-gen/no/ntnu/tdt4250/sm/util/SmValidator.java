/**
 */
package no.ntnu.tdt4250.sm.util;

import java.util.Map;

import no.ntnu.tdt4250.sm.*;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see no.ntnu.tdt4250.sm.SmPackage
 * @generated
 */
public class SmValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final SmValidator INSTANCE = new SmValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "no.ntnu.tdt4250.sm";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SmValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
		return SmPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		switch (classifierID) {
		case SmPackage.STATE:
			return validateState((State) value, diagnostics, context);
		case SmPackage.TRANSITION:
			return validateTransition((Transition) value, diagnostics, context);
		case SmPackage.STATE_MACHINE:
			return validateStateMachine((StateMachine) value, diagnostics, context);
		case SmPackage.FINAL_STATE:
			return validateFinalState((FinalState) value, diagnostics, context);
		case SmPackage.INITIAL_STATE:
			return validateInitialState((InitialState) value, diagnostics, context);
		default:
			return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateState(State state, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(state, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(state, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(state, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(state, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(state, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(state, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(state, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(state, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(state, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateState_stateNameShouldBeShorterThan10(state, diagnostics, context);
		return result;
	}

	/**
	 * Validates the stateNameShouldBeShorterThan10 constraint of '<em>State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateState_stateNameShouldBeShorterThan10(State state, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		// TODO implement the constraint
		// -> specify the condition that violates the constraint
		// -> verify the diagnostic details, including severity, code, and message
		// Ensure that you remove @generated or mark it @generated NOT
		boolean valid = true;
		
		if(state.getName() != null) {
			if(state.getName().length() >= 10) {
				valid = false;
			}
		}
		
		if (!valid) {
			if (diagnostics != null) {
				diagnostics.add(
						createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0, "_UI_GenericConstraint_diagnostic",
								new Object[] { "stateNameShouldBeShorterThan10", getObjectLabel(state, context) },
								new Object[] { state }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransition(Transition transition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transition, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStateMachine(StateMachine stateMachine, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(stateMachine, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(stateMachine, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(stateMachine, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(stateMachine, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(stateMachine, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(stateMachine, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(stateMachine, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(stateMachine, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(stateMachine, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateStateMachine_initialStateShouldNotHaveIncomingTransitions(stateMachine, diagnostics,
					context);
		return result;
	}

	/**
	 * Validates the initialStateShouldNotHaveIncomingTransitions constraint of '<em>State Machine</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateStateMachine_initialStateShouldNotHaveIncomingTransitions(StateMachine stateMachine,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO implement the constraint
		// -> specify the condition that violates the constraint
		// -> verify the diagnostic details, including severity, code, and message
		// Ensure that you remove @generated or mark it @generated NOT
		boolean valid = true;
		
		if(stateMachine.getInitialState().getIncoming().size() > 0) {
			valid = false;
		}
		
		if (!valid) {
			if (diagnostics != null) {
				diagnostics.add(createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0,
						"_UI_GenericConstraint_diagnostic", new Object[] {
								"initialStateShouldNotHaveIncomingTransitions", getObjectLabel(stateMachine, context) },
						new Object[] { stateMachine }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFinalState(FinalState finalState, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(finalState, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(finalState, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(finalState, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(finalState, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(finalState, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(finalState, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(finalState, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(finalState, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(finalState, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateState_stateNameShouldBeShorterThan10(finalState, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateInitialState(InitialState initialState, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(initialState, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(initialState, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(initialState, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(initialState, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(initialState, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(initialState, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(initialState, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(initialState, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(initialState, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateState_stateNameShouldBeShorterThan10(initialState, diagnostics, context);
		return result;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //SmValidator
