/**
 */
package no.ntnu.tdt4250.sm;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Initial State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see no.ntnu.tdt4250.sm.SmPackage#getInitialState()
 * @model
 * @generated
 */
public interface InitialState extends State {
} // InitialState
