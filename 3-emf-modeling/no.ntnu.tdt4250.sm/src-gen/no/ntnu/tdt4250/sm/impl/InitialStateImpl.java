/**
 */
package no.ntnu.tdt4250.sm.impl;

import no.ntnu.tdt4250.sm.InitialState;
import no.ntnu.tdt4250.sm.SmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Initial State</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InitialStateImpl extends StateImpl implements InitialState {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InitialStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SmPackage.Literals.INITIAL_STATE;
	}

} //InitialStateImpl
