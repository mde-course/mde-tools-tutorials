/**
 */
package no.ntnu.tdt4250.sm;

import java.math.BigInteger;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State Machine</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link no.ntnu.tdt4250.sm.StateMachine#getName <em>Name</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.sm.StateMachine#getDescription <em>Description</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.sm.StateMachine#getState <em>State</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.sm.StateMachine#getTransition <em>Transition</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.sm.StateMachine#getInitialState <em>Initial State</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.sm.StateMachine#getNumberOfStates <em>Number Of States</em>}</li>
 * </ul>
 *
 * @see no.ntnu.tdt4250.sm.SmPackage#getStateMachine()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='initialStateShouldNotHaveIncomingTransitions'"
 * @generated
 */
public interface StateMachine extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see no.ntnu.tdt4250.sm.SmPackage#getStateMachine_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link no.ntnu.tdt4250.sm.StateMachine#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see no.ntnu.tdt4250.sm.SmPackage#getStateMachine_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link no.ntnu.tdt4250.sm.StateMachine#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>State</b></em>' containment reference list.
	 * The list contents are of type {@link no.ntnu.tdt4250.sm.State}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' containment reference list.
	 * @see no.ntnu.tdt4250.sm.SmPackage#getStateMachine_State()
	 * @model containment="true" lower="2"
	 * @generated
	 */
	EList<State> getState();

	/**
	 * Returns the value of the '<em><b>Transition</b></em>' containment reference list.
	 * The list contents are of type {@link no.ntnu.tdt4250.sm.Transition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition</em>' containment reference list.
	 * @see no.ntnu.tdt4250.sm.SmPackage#getStateMachine_Transition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Transition> getTransition();

	/**
	 * Returns the value of the '<em><b>Initial State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial State</em>' reference.
	 * @see no.ntnu.tdt4250.sm.SmPackage#getStateMachine_InitialState()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	State getInitialState();

	/**
	 * Returns the value of the '<em><b>Number Of States</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of States</em>' attribute.
	 * @see no.ntnu.tdt4250.sm.SmPackage#getStateMachine_NumberOfStates()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Integer" required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	BigInteger getNumberOfStates();

} // StateMachine
