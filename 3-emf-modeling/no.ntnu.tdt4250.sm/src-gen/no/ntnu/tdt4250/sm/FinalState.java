/**
 */
package no.ntnu.tdt4250.sm;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Final State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see no.ntnu.tdt4250.sm.SmPackage#getFinalState()
 * @model
 * @generated
 */
public interface FinalState extends State {
} // FinalState
