/**
 */
package no.ntnu.tdt4250.sm;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link no.ntnu.tdt4250.sm.State#getName <em>Name</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.sm.State#getOutgoing <em>Outgoing</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.sm.State#getIncoming <em>Incoming</em>}</li>
 * </ul>
 *
 * @see no.ntnu.tdt4250.sm.SmPackage#getState()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='stateNameShouldBeShorterThan10'"
 * @generated
 */
public interface State extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see no.ntnu.tdt4250.sm.SmPackage#getState_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link no.ntnu.tdt4250.sm.State#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Outgoing</b></em>' reference list.
	 * The list contents are of type {@link no.ntnu.tdt4250.sm.Transition}.
	 * It is bidirectional and its opposite is '{@link no.ntnu.tdt4250.sm.Transition#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing</em>' reference list.
	 * @see no.ntnu.tdt4250.sm.SmPackage#getState_Outgoing()
	 * @see no.ntnu.tdt4250.sm.Transition#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<Transition> getOutgoing();

	/**
	 * Returns the value of the '<em><b>Incoming</b></em>' reference list.
	 * The list contents are of type {@link no.ntnu.tdt4250.sm.Transition}.
	 * It is bidirectional and its opposite is '{@link no.ntnu.tdt4250.sm.Transition#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming</em>' reference list.
	 * @see no.ntnu.tdt4250.sm.SmPackage#getState_Incoming()
	 * @see no.ntnu.tdt4250.sm.Transition#getTarget
	 * @model opposite="target"
	 * @generated
	 */
	EList<Transition> getIncoming();

} // State
